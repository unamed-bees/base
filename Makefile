.PHONY: down logs packages reboot restart run shell up

args = $(foreach a,$($(subst -,_,$1)_args),$(if $(value $a),$a="$($a)"))

down:
	@docker compose down ${@:2}

logs:
	@docker compose logs -f ${@:2}

packages:
	@echo "Installing packages for backend"
	@docker compose run --rm backend npm install

restart:
	@docker-compose down
	@docker-compose up -d

run:
	@docker compose up ${@:2}

up:
	@docker compose up ${@:2} -d

populate-db:
	@docker compose exec backend node dist/console db:seed

reset: populate-db
